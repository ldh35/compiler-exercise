# Compiler Exercise

An exploration into LR compilers, written in Java.

## About

Examples of usage can be found in `Main.java`. 

Specifically, one can create a simplistic LR compiler as so:

### Defining the Grammar:

To create a `Grammar` object, perform the following:

- Design the context-free grammar for the compiler.
- Create a single `Terminal` object for each terminal in the grammar. Supply it with:
  - The datatype that the terminal should be parsed to (supplied as the generic type of the terminal, `Res`). This type should be `Void` if the terminal should not be parsed into a datatype.
  - A user-readable name.
  - The regex which matches the terminal. The regex should not match the empty string.
  - Optionally, a functor with fingerprint `Res ( String )`, which parses a matched string into the specified datatype.
- Create a single `NonTerminal` object for each non-terminal in the grammar. Supply it with:
  - The datatype associated with the non-terminal (supplied as the generic type of the terminal, `Res`). Again, this type can be `Void`.
  - A user-readable name.
- For each production, call `NonTerminal.add_production` on the `NonTerminal` object for its head. Supply it with:
  - A list of `GrammarSymbol` objects that make up the body. For an empty production, supply the empty list.
  - Optionally, a functor with fingerprint `Res ( List<? extends Token<?>> )`, which accepts a list of tokens and reduces them to an object of type `Res`. This functor will be invoked when a reduction by this production occurs.
- Create a `Grammar` object, supplying it with the start non-terminal of the grammar.

Note that a `Token<Res>` object is a pair, consisting of a `value` attribute of type `Res` and a `GrammarSymbol<Res>` object, which specifies which grammar symbol the value is associated with.

### Creating the Automaton:

To create the automaton from a previously defined `Grammar` object, either construct an `SLRAutomaton` or `LR1Automaton`, supplying the grammar to the constructor. Both of these classes are concrete subtypes of the abstract `LRAutomaton` class.

The states of the automaton are automatically generated, and any conflict that arises (such as a _shift/reduce_ or _reduce/reduce_ conflict) will be reported by means of a `NonLRGrammarException`.

### Running the Automaton:

The `LRAutomaton.parse` method accepts a `StringBuffer` object, which is the plain-text sentence to parse. On successful parsing, an object of type `Res` is returned, which is the output of the reducer functor of the start symbol upon parsing completion.

Lexical Analysis occurs implicitly, where the `Terminal` whose regex matches the longest substring at the start of `StringBuffer` is decided as the next symbol.

On parsing failure, an `LRParseException` is thrown.

Regardless of the outcome of the previous call to `LRAutomaton.parse`, the automaton will be left in a clean state, ready for another parse.