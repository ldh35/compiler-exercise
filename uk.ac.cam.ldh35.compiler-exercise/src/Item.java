import java.util.*;

/**
 * A class storing an LR(0) or LR(1) item.
 */
public class Item<Res>
{
	/** The core of the item */
	public final NonTerminal<Res>.Production core;

	/** The position of the dot */
	public final int dot;

	public final Set<Terminal<?>> lookaheads;



	/**
	 * @param core  The production associated with the item.
	 * @param dot   The position of the dot.
	 * @param lookaheads  A set of lookahead terminals that must proceed the item for a reduction to be valid.
	 */
	public Item ( NonTerminal<Res>.Production core, int dot, Set<Terminal<?>> lookaheads )
	{
		/* Set the attributes */
		this.core = core;
		this.dot = dot;
		this.lookaheads = new HashSet<> ( lookaheads );

		/* Assertions */
		if ( dot < 0 || dot > core.body.size () )
			throw new IndexOutOfBoundsException ( "Item.Item: Dot is out of bounds" );
	}

	/**
	 * @param core  The production associated with the item.
	 * @param dot   The position of the dot.
	 */
	public Item ( NonTerminal<Res>.Production core, int dot )
	{
		this ( core, dot, new HashSet<> () );
	}

	/**
	 * @param other Another item to copy into this one.
	 */
	public Item ( Item<Res> other )
	{
		this.core = other.core;
		this.dot = other.dot;
		this.lookaheads = new HashSet<> ( other.lookaheads );
	}



	/**
	 * @return An unmodifiable view of the symbols after the dot.
	 */
	public List<GrammarSymbol<?>> afterDot ()
	{
		return Collections.unmodifiableList ( core.body.subList ( dot, core.body.size () ) );
	}


	/**
	 * @return An item with the dot shifted one to the right, or throws IndexOutOfBoundsException if the dot is at the far right end already.
	 */
	public Item<Res> shiftDot ()
	{
		return new Item<> ( core, dot + 1, lookaheads );
	}


	/**
	 * @return True iff the dot is at the end of the body.
	 */
	public boolean dotAtEnd ()
	{
		return dot == core.body.size ();
	}



	/**
	 * @return The item formatted as a string.
	 */
	@Override
	public String toString ()
	{
		/* Create the string builder and append the head of the core */
		StringBuilder str = new StringBuilder ();
		str.append ( "[ " ).append ( core.head.getName () ).append ( " ->" );

		/* Iterate through the body of the core, putting the dot in the correct place */
		for ( int i = 0; i < core.body.size (); ++i )
		{
			if ( i == dot ) str.append ( " \u2022" );
			str.append ( " " ).append ( core.body.get ( i ).getName () );
		}
		if ( dot == core.body.size () ) str.append ( " \u2022" );


		/* Add a gap followed by the set of lookaheads, if there are any */
		if ( !lookaheads.isEmpty () )
		{
			str.append ( "   |  " );
			for ( Terminal<?> lookahead : lookaheads )
			{
				str.append ( " " ).append ( lookahead.getName () );
			}
		}

		/* Add the final bracket and return */
		str.append ( " ]" );
		return str.toString ();
	}



	/**
	 * @param o Another item to compare to.
	 * @return Two items are equal if they have the same core and lookahead set.
	 */
	@Override
	public boolean equals ( Object o )
	{
		/* Check the type */
		if ( ! ( o instanceof Item ) )
			return false;

		/* Cast */
		@SuppressWarnings ( "unchecked" )
		Item<Res> item = (Item<Res>) o;

		/* Compare */
		return core == item.core && dot == item.dot && lookaheads.equals ( item.lookaheads );
	}



	/**
	 * @return A hashcode for the object.
	 */
	@Override
	public int hashCode ()
	{
		return Objects.hash ( core, dot, lookaheads );
	}
}
