import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Main
{
	public static Grammar<String> mathGrammar ()
	{
		final var decimal = new Terminal<Double> ( "DEC", "[0-9]+(\\.[0-9]*)?|\\.[0-9]+", Double::parseDouble );
		final var newline = new Terminal<Void> ( "\\n", "\\n", null );
		final var comma = new Terminal<Void> ( ",", ",", null );
		final var add = new Terminal<Void> ( "+", "\\+", null );
		final var sub = new Terminal<Void> ( "-", "\\-", null );
		final var mult = new Terminal<Void> ( "*", "\\*", null );
		final var div = new Terminal<Void> ( "/", "/", null );
		final var exp = new Terminal<Void> ( "exp", "exp", null );
		final var exp2 = new Terminal<Void> ( "exp2", "exp2", null );
		final var exp3 = new Terminal<Void> ( "exp3", "exp3", null );
		final var sqrt = new Terminal<Void> ( "sqrt", "sqrt", null );
		final var pow = new Terminal<Void> ( "pow", "pow", null );
		final var factorial = new Terminal<Void> ( "!", "\\!", null );
		final var openbracket = new Terminal<Void> ( "(", "\\(", null );
		final var closebracket = new Terminal<Void> ( ")", "\\)", null );

		final var lines = new NonTerminal<String> ( "lines" );
		final var line = new NonTerminal<String> ( "line" );
		final var t0 = new NonTerminal<Double> ( "t0" );
		final var t1 = new NonTerminal<Double> ( "t1" );
		final var t2 = new NonTerminal<Double> ( "t2" );
		final var t3 = new NonTerminal<Double> ( "t3" );
		final var t4 = new NonTerminal<Double> ( "t4" );
		final var t5 = new NonTerminal<Double> ( "t5" );
		final var t6 = new NonTerminal<Double> ( "t6" );
		final var t7 = new NonTerminal<Double> ( "t7" );
		final var t8 = new NonTerminal<Double> ( "t8" );

		lines.addProduction ( List.of ( line, lines ), ( body ) -> (String) body.get ( 0 ).value + "\n" + (String) body.get ( 1 ).value );
		lines.addProduction ( List.of (), ( body ) -> "" );
		line.addProduction ( List.of ( t0, newline ), ( body ) -> ( (Double) body.get ( 0 ).value ).toString () );

		t0.addProduction ( List.of ( t1 ), ( body ) -> (Double) body.get ( 0 ).value );
		t0.addProduction ( List.of ( t0, add, t1 ), ( body ) -> (Double) body.get ( 0 ).value + (Double) body.get ( 2 ).value );

		t1.addProduction ( List.of ( t2 ), ( body ) -> (Double) body.get ( 0 ).value );
		t1.addProduction ( List.of ( t1, sub, t2 ), ( body ) -> (Double) body.get ( 0 ).value - (Double) body.get ( 2 ).value );

		t2.addProduction ( List.of ( t3 ), ( body ) -> (Double) body.get ( 0 ).value );
		t2.addProduction ( List.of ( t2, mult, t3 ), ( body ) -> (Double) body.get ( 0 ).value * (Double) body.get ( 2 ).value );

		t3.addProduction ( List.of ( t4 ), ( body ) -> (Double) body.get ( 0 ).value );
		t3.addProduction ( List.of ( t3, div, t4 ), ( body ) -> (Double) body.get ( 0 ).value / (Double) body.get ( 2 ).value );

		t4.addProduction ( List.of ( t5 ), ( body ) -> (Double) body.get ( 0 ).value );
		t4.addProduction ( List.of ( add, t4 ), ( body ) -> (Double) body.get ( 1 ).value );
		t4.addProduction ( List.of ( sub, t4 ), ( body ) -> - (Double) body.get ( 1 ).value );

		t5.addProduction ( List.of ( t6 ), ( body ) -> (Double) body.get ( 0 ).value );
		t5.addProduction ( List.of ( t5, factorial ), ( body ) -> { double out = 1; for ( int i = 2; i <= (Double) body.get ( 0 ).value && out != Double.POSITIVE_INFINITY; ++i ) out *= i; return out; } );

		t6.addProduction ( List.of ( t7 ), ( body ) -> (Double) body.get ( 0 ).value );
		t6.addProduction ( List.of ( exp, t6 ), ( body ) -> Math.exp ( (Double) body.get ( 1 ).value ) );
		t6.addProduction ( List.of ( exp2, t6 ), ( body ) -> Math.pow ( 2, (Double) body.get ( 1 ).value ) );
		t6.addProduction ( List.of ( exp3, t6 ), ( body ) -> Math.pow ( 3, (Double) body.get ( 1 ).value ) );
		t6.addProduction ( List.of ( sqrt, t6 ), ( body ) -> Math.sqrt ( (Double) body.get ( 1 ).value ) );
		t6.addProduction ( List.of ( pow, openbracket, t0, comma, t0, closebracket ), ( body ) -> Math.pow ( (Double) body.get ( 2 ).value, (Double) body.get ( 4 ).value ) );

		t7.addProduction ( List.of ( t8 ), ( body ) -> (Double) body.get ( 0 ).value );
		t7.addProduction ( List.of ( decimal ), ( body ) -> (Double) body.get ( 0 ).value );
		t7.addProduction ( List.of ( openbracket, t0, closebracket ), ( body ) -> (Double) body.get ( 1 ).value );

		return new Grammar<> ( lines );
	}


	public static Grammar<Void> nonSLRGrammar ()
	{
		final var LINE = new NonTerminal<Void> ( "LINE" );

		final var S = new NonTerminal<Void> ( "S" );
		final var L = new NonTerminal<Void> ( "L" );
		final var R = new NonTerminal<Void> ( "R" );

		final var ID = new Terminal<Void> ( "ID", "[a-zA-Z][a-zA-Z0-9]*" );
		final var EQ = new Terminal<Void> ( "=", "\\=" );
		final var STAR = new Terminal<Void> ( "*", "\\*" );
		final var NEWLINE = new Terminal<Void> ( "\\n", "\\n" );

		LINE.addProduction ( List.of ( S, NEWLINE ) );

		S.addProduction ( List.of ( L, EQ, R ) );
		S.addProduction ( List.of ( R ) );

		L.addProduction ( List.of ( STAR, R ) );
		L.addProduction ( List.of ( ID ) );

		R.addProduction ( List.of ( L ) );

		return new Grammar<> ( LINE );
	}


	public static void main ( String[] args ) throws IOException
	{
		LRAutomaton<String> automaton = new LR1Automaton<> ( mathGrammar () );

		BufferedReader in = new BufferedReader ( new InputStreamReader ( System.in ) );
		while ( true )
			try
			{
				System.out.println ( automaton.parse ( new StringBuffer ( in.readLine () + "\n" ) ) );
			} catch ( LRParseException e )
			{
				System.out.println ( e.getMessage () );
			}
	}
}