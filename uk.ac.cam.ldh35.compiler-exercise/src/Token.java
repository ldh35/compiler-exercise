import java.util.Objects;

/**
 * A grammar symbol with associated value.
 */
public class Token<Res>
{

	/** The grammar symbol of the token */
	public final GrammarSymbol<Res> symbol;

	/** The value of the token */
	public final Res value;



	/** The end token */
	public static final Token<Void> end = new Token<> ( Terminal.end, null );



	/**
	 * @param symbol    The symbol the token is for.
	 * @param value The value of the token.
	 */
	Token ( GrammarSymbol<Res> symbol, Res value )
	{
		this.symbol = symbol;
		this.value = value;
	}

	/**
	 * @param other Another token to copy into this one.
	 */
	Token ( Token<Res> other )
	{
		this.symbol = other.symbol;
		this.value = other.value;
	}


	@Override
	public String toString ()
	{
		return symbol.getName () + " = " + value;
	}



	/**
	 * @param o Another token to compare to.
	 * @return Two tokens are equal if they are for the same grammar symbol, and their values are equal.
	 */
	@Override
	public boolean equals ( Object o )
	{
		/* Check the type */
		if ( ! ( o instanceof Token ) )
			return false;

		/* Cast */
		@SuppressWarnings ( "unchecked" )
		Token<Res> token = (Token<Res>) o;

		/* Compare */
		return symbol == token.symbol && Objects.equals ( this.value, token.value );
	}



	/**
	 * @return A hashcode for the object.
	 */
	@Override
	public int hashCode ()
	{
		return Objects.hash ( symbol, value );
	}
}
