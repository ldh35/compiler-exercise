/**
 * An exception for errors when parsing a sentence.
 */
public class LRParseException extends RuntimeException
{
	/**
	 * Construct an exception without a message.
	 */
	LRParseException ()
	{
		super ();
	}

	/**
	 * @param message The message for the exception.
	 */
	LRParseException ( String message )
	{
		super ( message );
	}
}
