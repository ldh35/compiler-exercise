import java.io.*;
import java.nio.CharBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class for a terminal grammar symbol.
 *
 * @param <Res> The resolution type of the GrammarSymbol.
 */
public class Terminal<Res> implements GrammarSymbol<Res>
{

	/** A functional interface for a parser */
	public interface Parser<Res>
	{
		Res parse ( String s );
	}



	/** A name for the terminal */
	public final String name;

	/** The regex that matches the terminal */
	public final String regex;

	/** A parsing function for the terminal */
	public final Parser<Res> parser;

	/** Compiled regex for the terminal */
	private final Pattern pattern;



	/** An empty terminal */
	public final static Terminal<Void> empty = new Terminal<> ( "\u03b5", "", null, null );

	/** The end terminal */
	public final static Terminal<Void> end = new Terminal<> ( "$", "$", null, null );

	/** A placeholder terminal */
	public final static Terminal<Void> placeholder = new Terminal<> ( "#", "", null, null );



	/**
	 * @param name  A name for the terminal.
	 * @param regex Regex that matches the symbol.
	 * @param parser A function to parse a matched symbol to type Res.
	 */
	private Terminal ( String name, String regex, Parser<Res> parser, Void priv )
	{
		/* Save the attributes */
		this.name = name;
		this.regex = regex;
		this.parser = parser;

		/* Compile the regex */
		this.pattern = Pattern.compile ( "^(" + regex + ")" );
	}


	/**
	 * @param name  A name for the terminal.
	 * @param regex Regex that matches the symbol.
	 * @param parser A function to parse a matched symbol to type Res.
	 */
	public Terminal ( String name, String regex, Parser<Res> parser )
	{
		/* Save the attributes */
		this.name = name;
		this.regex = regex;
		this.parser = parser;

		/* Compile the regex */
		this.pattern = Pattern.compile ( "^(" + regex + ")" );

		/* If the terminal can be empty, throw */
		if ( pattern.matcher ( "" ).matches () )
			throw new IllegalArgumentException ( "Terminal.Terminal: Terminal must not match the empty string (regex: '" + regex + "')" );
	}


	/**
	 * @param name  The name of the terminal.
	 * @param regex The regex of the terminal.
	 */
	public Terminal ( String name, String regex )
	{
		this ( name, regex, null );
	}



	/**
	 * @return Whether the grammar symbol is a terminal.
	 */
	@Override
	public boolean isTerminal () { return true; }

	/**
	 * @return The name of the grammar symbol.
	 */
	@Override
	public String getName ()
	{
		return name;
	}



	/**
	 * @param st    The input stream to match from.
	 * @return The String on top of the stream which matches.
	 */
	public String matchStream ( StringBuffer st )
	{
		/* Create the matcher */
		Matcher m = pattern.matcher ( st.toString () );

		/* Return empty if no match was made */
		if ( !m.find () ) return "";

		/* Return the match */
		return m.group ();
	}



	/**
	 * @param st    The input stream to match from.
	 * @return  An object of type TerminalToken<Res> if matching was successful, otherwise null.
	 */
	public TerminalToken<Res> parseStream ( StringBuffer st )
	{
		/* Create the matcher */
		Matcher m = pattern.matcher ( st.toString () );

		/* Return null if no match was made */
		if ( !m.find () ) return null;

		/* Otherwise, get the match and remove it from the buffer */
		String match = m.group ();
		st.replace ( 0, match.length (), "" );

		/* Parse the match */
		return new TerminalToken<> ( this, parser == null ? null : parser.parse ( match ) );
	}


	/**
	 * @return Print the terminal name and associated regex.
	 */
	@Override
	public String toString ()
	{
		return name + " ~ '" + regex + "'";
	}
}
