/**
 * A token whose symbol is known to be a non-terminal.
 *
 * @param <Res> The type of the token.
 */
public class NonTerminalToken<Res> extends Token<Res>
{
	/** The terminal for the token */
	public final NonTerminal<Res> symbol;

	/**
	 * @param symbol The symbol the token is for.
	 * @param value  The value of the token.
	 */
	NonTerminalToken ( NonTerminal<Res> symbol, Res value )
	{
		super ( symbol, value );
		this.symbol = symbol;
	}

	/** A copy constructor */
	NonTerminalToken ( NonTerminalToken<Res> other )
	{
		super ( other );
		this.symbol = other.symbol;
	}
}
