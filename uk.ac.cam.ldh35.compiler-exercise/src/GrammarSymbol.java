/**
 * An interface for a grammar symbol.
 *
 * @param <Res> The resolution type of the GrammarSymbol.
 */
public interface GrammarSymbol<Res>
{

	/**
	 * @return Whether the grammar symbol is a terminal.
	 */
	boolean isTerminal ();


	/**
	 * @return The name of the grammar symbol.
	 */
	String getName ();
}
