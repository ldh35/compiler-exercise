import java.util.*;

/**
 * A class encapsulating an LR automaton.
 */
public abstract class LRAutomaton<Res>
{

	/** A state of the automaton */
	protected static class State
	{
		/**
		 * An action enumeration.
		 */
		public class Action
		{
			/** An enumeration of what to do */
			enum Operation { REDUCE, SHIFT, ACCEPT, ERROR }

			/** The operation to perform */
			public final Operation operation;

			/** The next state */
			public final State shiftState;

			/** The production to reduce by (if the action type is reduce) */
			public final NonTerminal<?>.Production reduceBy;


			/**
			 * @param other The action to compare to.
			 */
			public void throwIfNoMatch ( Action other )
			{
				/* Return if the other action is null */
				if ( other == null ) return;

				/* Check they are the same operation */
				if ( operation == other.operation )
					switch ( operation )
					{
						/* Compare reduce operations */
						case REDUCE -> { if ( reduceBy == other.reduceBy ) return; }
						case SHIFT  -> { if ( shiftState == other.shiftState ) return; }
						default -> { return; }
					}

				/* We have an error */
				throw new NonLRGrammarException ( operation + "/" + other.operation + " conflict on state " + State.this );
			}



			/**
			 * @param operation The operation to perform.
			 * @param shiftState The goto state.
			 * @param reduceBy  The production to reduce by (if the action type is reduce).
			 */
			public Action ( Operation operation, State shiftState, NonTerminal<?>.Production reduceBy )
			{
				this.operation = operation;
				this.shiftState = shiftState;
				this.reduceBy = reduceBy;
			}
		}


		/** A set of items representing the state */
		public final Set<Item<?>> items;

		/** A map of terminals to actions */
		public final Map<Terminal<?>, Action> actions;

		/** A map of non-terminals to states */
		public final Map<NonTerminal<?>, State> gotoStates;


		/**
		 * @param items   A set of items representing the state.
		 */
		State ( Set<Item<?>> items )
		{
			this.items = new ItemSet ( items );
			this.actions = new HashMap<> ();
			this.gotoStates = new HashMap<> ();
		}



		/**
		 * @return The state, formatted as a string.
		 */
		@Override
		public String toString ()
		{
			return "STATE: " + items;
		}


		/**
		 * @param terminal  The terminal to add the action for.
		 * @param newAction The action to add.
		 * @return True iff the action was newly added.
		 */
		private boolean addAction ( Terminal<?> terminal, Action newAction )
		{
			Action oldAction = actions.putIfAbsent ( terminal, newAction );
			newAction.throwIfNoMatch ( oldAction );
			return oldAction == null;
		}


		/**
		 * @param terminal  The terminal to add the action for.
		 * @param reduceBy  The production to reduce by.
		 * @return True iff the action was newly added.
		 */
		public boolean addReduceAction ( Terminal<?> terminal, NonTerminal<?>.Production reduceBy )
		{
			Action newAction = new Action ( Action.Operation.REDUCE, null, reduceBy );
			return addAction ( terminal, newAction );
		}

		/**
		 * @param terminal  The terminal to add the action for.
		 * @param shiftState The shift state.
		 * @return True iff the action was newly added.
		 */
		public boolean addShiftAction ( Terminal<?> terminal, State shiftState )
		{
			Action newAction = new Action ( Action.Operation.SHIFT, shiftState, null );
			return addAction ( terminal, newAction );
		}

		/**
		 * @param terminal  The terminal to add the action for.
		 * @return True iff the action was newly added.
		 */
		public boolean addAcceptAction ( Terminal<?> terminal )
		{
			Action newAction = new Action ( Action.Operation.ACCEPT, null, null );
			return addAction ( terminal, newAction );
		}

		/**
		 * @param terminal  The terminal to add the action for.
		 * @return True iff the action was newly added.
		 */
		public boolean addErrorAction ( Terminal<?> terminal )
		{
			Action newAction = new Action ( Action.Operation.ERROR, null, null );
			return addAction ( terminal, newAction );
		}


		/**
		 * @param nonTerminal   The non-terminal to add the goto state for.
		 * @param gotoState The state to add.
		 * @return True iff the state was newly added.
		 */
		public boolean addGoto ( NonTerminal<?> nonTerminal, State gotoState )
		{
			State oldState = gotoStates.putIfAbsent ( nonTerminal, gotoState );
			if ( oldState != null && oldState != gotoState )
				throw new NonLRGrammarException ( "GOTO conflict on state " + super.toString () );
			return oldState == null;
		}
	}


	/**
	 * A class for a token with an associated automaton state.
	 */
	protected static class ConsumedToken<Res> extends Token<Res>
	{
		/** The state associated with the active state */
		public final State state;


		/**
		 * @param symbol    The symbol the token is for.
		 * @param value The value of the token.
		 * @param state The state associated with the consumed token.
		 */
		ConsumedToken ( GrammarSymbol<Res> symbol, Res value, State state )
		{
			super ( symbol, value );
			this.state = state;
		}


		/**
		 * @param token The consumed token.
		 * @param state The state associated with the consumed token.
		 */
		ConsumedToken ( Token<Res> token, State state )
		{
			super ( token );
			this.state = state;
		}
	}



	/** The grammar of the automaton */
	public final Grammar<Res> grammar;

	/** The start state of the automaton */
	private final State startState;


	/**
	 * @param grammar   The grammar of the automaton.
	 */
	public LRAutomaton ( Grammar<Res> grammar )
	{
		/* Save the grammar */
		this.grammar = grammar;

		/* Set up the automaton */
		this.startState = setup ();
	}


	/**
	 * @param st    The string to parse.
	 * @return The result of the parsing, or throws.
	 */
	public Res parse ( String st )
	{
		return parse ( new StringBuffer ( st ) );
	}

	/**
	 * @param st    The string buffer to parse.
	 * @return  The result of the parsing, or throws.
	 */
	public Res parse ( StringBuffer st )
	{
		/* Create a configuration */
		List<ConsumedToken<?>> stack = new ArrayList<> ();

		/* Add the initial state */
		stack.add ( new ConsumedToken<> ( grammar.start, null, startState ) );

		/* The token as a result of matching */
		TerminalToken<?> token = null;

		/* Iterate over getting terminals out of the string and parsing them */
		while ( true )
		{
			/* Get the next token if it is null */
			if ( token == null )
			{
				/* Strip any whitespace */
				while ( !st.isEmpty () && st.charAt ( 0 ) == ' ' )
					st.replace ( 0, 1, "" );

				/* If the buffer is empty, produce the end marker.
				 * Otherwise, try to match each non-terminal to the buffer.
				 */
				if ( st.isEmpty () )
					token = TerminalToken.end;
				else
				{
					/* We want to remember the terminal with the longest match */
					int longestMatch = 0;
					Terminal<?> longestMatchTerminal = null;

					/* Iterate over the terminals and update the longest match */
					for ( Terminal<?> terminal : grammar.terminals )
					{
						int matchLength = terminal.matchStream ( st ).length ();
						if ( matchLength > longestMatch )
						{
							longestMatch = matchLength;
							longestMatchTerminal = terminal;
						}
					}

					/* Get the token, if a match was found */
					if ( longestMatchTerminal != null )
						token = longestMatchTerminal.parseStream ( st );
				}

				/* Throw if no token was found */
				if ( token == null )
					throw new LRParseException ( "Failed to find a terminal in remaining text: '" + st + "'" );
			}

			/* Get the state and action */
			State state = stack.get ( stack.size () - 1 ).state;
			State.Action action = state.actions.get ( token.symbol );

			/* Consider the action on receiving the token */
			switch ( action.operation )
			{
				/* The case where we need to shift */
				case SHIFT ->
				{
					/* Add the new state on the stack */
					stack.add ( new ConsumedToken<> ( token, action.shiftState ) );

					/* Consume the token */
					token = null;
				}

				/* The case where we need to reduce */
				case REDUCE ->
				{
					/* Try to reduce the stack */
					NonTerminalToken<?> reduction = action.reduceBy.parseStack ( stack );

					/* Throw if the reduction failed */
					if ( reduction == null )
						throw new LRParseException ( "Failed to reduce by " + action.reduceBy + " on state " + state );

					/* Get the goto state */
					State gotoState = stack.get ( stack.size () - 1 ).state.gotoStates.get ( reduction.symbol );

					/* Set the new top of the stack */
					stack.add ( new ConsumedToken<> ( reduction, gotoState ) );
				}

				/* The case where we need to accept */
				case ACCEPT ->
				{
					/* Return the top of the stack */
					return ( Res ) stack.get ( 1 ).value;
				}

				/* The error case */
				case ERROR ->
				{
					/* Throw a new exception */
					throw new LRParseException ( "Error action at state " + state + " on token " + token );
				}
			}
		}
	}



	/**
	 * @return The start state of the automaton.
	 */
	protected abstract State setup ();



	/**
	 * @param items A set of items.
	 * @return The closure of the items.
	 */
	protected abstract Set<Item<?>> close ( Set<Item<?>> items );



	/**
	 * @param items A set of items.
	 * @return The kernel items in the set.
	 */
	protected Set<Item<?>> kernel ( Set<Item<?>> items )
	{
		/* Create a set to return */
		Set<Item<?>> output = new ItemSet ();

		/* Remove those items which are non-kernel */
		for ( Item<?> item : items )
			if ( item.dot != 0 || item.core == grammar.startProduction )
				output.add ( item );

		/* Return the kernel items */
		return output;
	}



	/**
	 * @param items The item set to calculate the goto for. The method is LR(0) and LR(1) agnostic.
	 * @param symbol    The symbol to skip over.
	 * @return  A kernel item set.
	 */
	protected Set<Item<?>> gotoSet ( Set<Item<?>> items, GrammarSymbol<?> symbol )
	{
		/* Create a new set for the output */
		Set<Item<?>> output = new ItemSet ();

		/* Iterate over the items in the set */
		for ( Item<?> item : items )
		{
			/* Get the symbols after the dot */
			List<GrammarSymbol<?>> afterDot = item.afterDot ();

			/* If the symbol immediately after the dot is the supplied symbol, then add a new item to the output */
			if ( afterDot.size () != 0 && afterDot.get ( 0 ) == symbol )
				output.add ( item.shiftDot () );
		}

		/* Return the output */
		return output;
	}

}
