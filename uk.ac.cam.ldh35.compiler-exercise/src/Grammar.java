import java.util.*;

/**
 * A class representing a grammar for an LALR compiler.
 */
public class Grammar<Res>
{

	/** The terminals, in precedence order */
	public final Set<Terminal<?>> terminals;

	/** The non-terminals, in precedence order */
	public final Set<NonTerminal<?>> nonTerminals;

	/** All symbols in the grammar */
	public final Set<GrammarSymbol<?>> symbols;

	/** The special start non-terminal */
	public final NonTerminal<Void> start;

	/** The special start production */
	public final NonTerminal<Void>.Production startProduction;

	/** First sets for the grammar */
	private final Map<GrammarSymbol<?>, Set<Terminal<?>>> firstSets;

	/** Follow sets for the grammar */
	private final Map<GrammarSymbol<?>, Set<Terminal<?>>> followSets;



	/**
	 * @param startSymbol   The start non-terminal.
	 */
	public Grammar ( NonTerminal<Res> startSymbol )
	{
		/* Create the terminal and non-terminal sets. */
		this.terminals = new HashSet<> ();
		this.nonTerminals = new HashSet<> ();
		this.symbols = new HashSet<> ();

		/* Create the special start terminal */
		this.start = new NonTerminal<> ( "start" );
		this.start.addProduction ( List.of ( startSymbol ), null );
		this.startProduction = this.start.productions.get ( 0 );

		/* Add the symbols */
		addSymbols ();

		/* Check that no special symbol is in the terminal set */
		if ( this.terminals.contains ( Terminal.empty ) || this.terminals.contains ( Terminal.end ) || this.terminals.contains ( Terminal.placeholder ) )
			throw new IllegalArgumentException ( "Grammar.Grammar: Terminal set contains a special terminal" );

		/* Check that the start symbol is in the non-terminal set */
		if ( !nonTerminals.contains ( startSymbol ) )
			throw new IllegalArgumentException ( "Grammar.Grammar: startSymbol is not a member of nonTerminals" );

		/* Set up the first sets */
		this.firstSets = new HashMap<> ();
		calculateFirstSets ();

		/* Set up the follow sets */
		this.followSets = new HashMap<> ();
		calculateFollowSets ();
	}


	/**
	 * Search the grammar for symbols and add them to the sets.
	 */
	private void addSymbols ()
	{
		/* Create a stack to examine */
		List<GrammarSymbol<?>> toExamine = new ArrayList<> ();
		toExamine.add ( this.start );

		/* Iterate over the stack */
		while ( !toExamine.isEmpty () )
		{
			/* Get the next symbol */
			GrammarSymbol<?> next = toExamine.remove ( toExamine.size () - 1 );

			/* Add it to the set of symbols, and only continue if it is new */
			if ( symbols.add ( next ) )
			{
				/* Switch depending on whether the symbol is a terminal or non-terminal */
				if ( next.isTerminal () )
					terminals.add ( ( Terminal<?> ) next );
				else
				{
					nonTerminals.add ( ( NonTerminal<?> ) next );
					for ( NonTerminal<?>.Production production : ( ( NonTerminal<?> ) next ).productions )
						toExamine.addAll ( production.body );
				}

			}
		}
	}



	/**
	 * Calculate the first set for each terminal and non-terminal.
	 */
	private void calculateFirstSets ()
	{
		/* Set a singleton set for each terminal */
		for ( Terminal<?> terminal : terminals )
			firstSets.put ( terminal, Set.of ( terminal ) );

		/* Also add a singleton for the empty and end terminals */
		firstSets.putIfAbsent ( Terminal.empty, Set.of ( Terminal.empty ) );
		firstSets.putIfAbsent ( Terminal.end, Set.of ( Terminal.end ) );

		/* Add empty sets for each non-terminal */
		for ( NonTerminal<?> nonTerminal : this.nonTerminals )
			this.firstSets.put ( nonTerminal, new HashSet<> () );

		/* Perform fixed point iteration to compute the first set */
		boolean change;
		do
		{
			/* Set there to have been no change in this iteration */
			change = false;

			/* Iterate through the non-terminals and their productions */
			for ( NonTerminal<?> nonTerminal : nonTerminals )
				for ( NonTerminal<?>.Production production : nonTerminal.productions )
				{
					/* Get the set for this production */
					Set<Terminal<?>> firstSet = firstSets.get ( production.head );

					/* Get whether the empty symbol has already been added */
					boolean firstSetContainsEmpty = firstSet.contains ( Terminal.empty );

					/* If the production is empty, just add the empty symbol */
					if ( production.body.isEmpty () )
						firstSet.add ( Terminal.empty );

					/* Otherwise, loop through the body of the production */
					else for ( GrammarSymbol<?> symbol : production.body )
						{
							/* If the first set did not already contain empty, remove it */
							if ( !firstSetContainsEmpty ) firstSet.remove ( Terminal.empty );

							/* Get the new first set to add and add it */
							Set<Terminal<?>> symbolFirstSet = firstSets.get ( symbol );
							change |= firstSet.addAll ( symbolFirstSet );

							/* If the added first set does not contain empty, break */
							if ( !symbolFirstSet.contains ( Terminal.empty ) ) break;
						}
				}
		} while ( change );
	}



	/**
	 * Calculate the follow set for each terminal and non-terminal.
	 */
	private void calculateFollowSets ()
	{
		/* Add empty sets for each symbol */
		for ( GrammarSymbol<?> symbol : symbols )
			followSets.put ( symbol, new HashSet<> () );

		/* Add the end symbol to the follow set of the start symbol */
		followSets.get ( start ).add ( Terminal.end );

		/* Perform fixed point iteration to compute the first set */
		boolean change;
		do
		{
			/* Set there to have been no change in this iteration */
			change = false;

			/* Iterate through the non-terminals and their productions, and the symbols within the production */
			for ( NonTerminal<?> nonTerminal : nonTerminals )
				for ( NonTerminal<?>.Production production : nonTerminal.productions )
					for ( int i = 0; i < production.body.size (); ++i )
					{
						/* Get the symbol and its follow set */
						GrammarSymbol<?> symbol = production.body.get ( i );
						Set<Terminal<?>> followSet = followSets.get ( symbol );

						/* If this is the last symbol in the body, add the follow set of the head to the follow set of the symbol.
						 * Otherwise, consider the next symbol in the body.
						 */
						if ( i == production.body.size () - 1 )
							change |= followSet.addAll ( followSets.get ( production.head ) );
						else
						{
							/* Get the next symbol and its first and follow sets */
							GrammarSymbol<?> nextSymbol = production.body.get ( i + 1 );
							Set<Terminal<?>> nextSymbolFirstSet = first ( nextSymbol );
							Set<Terminal<?>> nextSymbolFollowSet = followSets.get ( nextSymbol );

							/* Remove the empty symbol from the first set */
							boolean nextSymbolCanBeEmpty = nextSymbolFirstSet.remove ( Terminal.empty );

							/* Add the first set */
							change |= followSet.addAll ( nextSymbolFirstSet );

							/* If the next symbol can be empty, add its follow set to this follow set */
							if ( nextSymbolCanBeEmpty )
								change |= followSet.addAll ( nextSymbolFollowSet );
						}
					}
		} while ( change );
	}



	/**
	 * @param symbol    The symbol to get the first set for.
	 * @return  The first set.
	 */
	public Set<Terminal<?>> first ( GrammarSymbol<?> symbol )
	{
		return new HashSet<> ( firstSets.get ( symbol ) );
	}



	/**
	 * @param symbols   The grammar symbols to calculate the first set.
	 * @return  The first set for the symbols.
	 */
	public Set<Terminal<?>> first ( List<? extends GrammarSymbol<?>> symbols )
	{
		/* Create a set to populate */
		Set<Terminal<?>> firstSet = new HashSet<> ();

		/* Add the empty symbol */
		firstSet.add ( Terminal.empty );

		/* Populate the set */
		for ( GrammarSymbol<?> symbol : symbols )
		{
			/* Remove empty from the first set */
			firstSet.remove ( Terminal.empty );

			/* Get the first set of this symbol */
			Set<Terminal<?>> symbolFirstSet = firstSets.get ( symbol );
			firstSet.addAll ( symbolFirstSet );

			/* Break if the empty symbol is not in the set */
			if ( !symbolFirstSet.contains ( Terminal.empty ) ) break;
		}

		/* Return the set */
		return firstSet;
	}



	/**
	 * @param symbol    The symbol to get the follow set for.
	 * @return  The follow set.
	 */
	public Set<Terminal<?>> follow ( GrammarSymbol<?> symbol )
	{
		return new HashSet<> ( followSets.get ( symbol ) );
	}



	/**
	 * @return Format the grammar tokens and productions.
	 */
	@Override
	public String toString ()
	{
		/* Set up the string builder */
		StringBuilder str = new StringBuilder ();

		/* Add the terminals */
		str.append ( "TERMINALS:\n\n" );
		for ( Terminal<?> terminal : terminals )
			str.append ( terminal.toString () ).append ( "\n" );

		/* Add the productions */
		str.append ( "\nPRODUCTIONS:\n\n" );
		for ( NonTerminal<?> nonTerminal : nonTerminals )
			str.append ( nonTerminal.toString () );

		/* Return the string */
		return str.toString ();
	}
}
