/**
 * An exception for errors when generating an LR automaton.
 */
public class NonLRGrammarException extends RuntimeException
{
	/**
	 * Construct an exception without a message.
	 */
	NonLRGrammarException ()
	{
		super ();
	}

	/**
	 * @param message The message for the exception.
	 */
	NonLRGrammarException ( String message )
	{
		super ( message );
	}
}
