import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LR1Automaton<Res> extends LRAutomaton<Res>
{

	/**
	 * @param grammar   The grammar of the automaton.
	 */
	public LR1Automaton ( Grammar<Res> grammar )
	{
		super ( grammar );
	}



	/**
	 * @param items A set of LR(1) items.
	 * @return The LR(1) closure of the items.
	 */
	protected Set<Item<?>> close ( Set<Item<?>> items )
	{
		/* Create the new closure set */
		Set<Item<?>> closure = new ItemSet ( items );

		/* Loop while change occurs */
		boolean change;
		do
		{
			/* Copy over the closure set and set there to be no change */
			items = new ItemSet ( closure );
			change = false;

			/* Iterate over the items */
			for ( Item<?> item : items )
			{
				/* Get the symbols after the dot */
				List<GrammarSymbol<?>> afterDot = item.afterDot ();

				/* We can add to the closure if there is a non-terminal immediately after the dot */
				if ( afterDot.size () != 0 && !afterDot.get ( 0 ).isTerminal () )
				{
					/* Get the lookaheads to add and potentially remove the empty symbol */
					Set<Terminal<?>> newLookaheads = grammar.first ( afterDot.subList ( 1, afterDot.size () ) );
					boolean newLookaheadsContainedEmpty = newLookaheads.remove ( Terminal.empty );

					/* Iterate over the productions of the non-terminal and the item lookaheads */
					for ( NonTerminal<?>.Production production : ( ( NonTerminal<?> ) afterDot.get ( 0 ) ).productions )
						for ( Terminal<?> lookahead : item.lookaheads )
						{
							/* Create the new item */
							Item<?> newItem = production.createItem ( 0, newLookaheads );
							if ( newLookaheadsContainedEmpty )
								newItem.lookaheads.add ( lookahead );

							/* Add to the closure */
							change |= closure.add ( newItem );
						}
				}
			}
		} while ( change );

		/* Return the closure */
		return closure;
	}



	/**
	 * @return The start state of an LR(1) automaton.
	 */
	@Override
	protected State setup ()
	{
		/* Create a map of item sets to states */
		Map<Set<Item<?>>, State> stateSet = new HashMap<> ();

		/* Set the start state */
		State startState = new State ( close ( Set.of ( grammar.startProduction.createItem ( 0, Set.of ( Terminal.end ) ) ) ) );
		stateSet.put ( startState.items, startState );

		/* Loop while changes are made */
		boolean change;
		boolean stateAdded;
		do
		{
			/* Set there to be no change */
			change = false;
			stateAdded = false;

			/* Iterate over the states */
			for ( State state : stateSet.values () )
			{
				/* Iterate over the grammar symbols */
				for ( GrammarSymbol<?> symbol : grammar.symbols )
				{
					/* Get the goto set */
					Set<Item<?>> gotoItems = gotoSet ( state.items, symbol );

					/* If the set is non-empty, add the state */
					if ( !gotoItems.isEmpty () )
					{
						/* Close the items */
						gotoItems = close ( gotoItems );

						/* Add the new state */
						change |= stateAdded |= stateSet.putIfAbsent ( gotoItems, new State ( gotoItems ) ) == null;

						/* Get the state just added */
						State addedState = stateSet.get ( gotoItems );

						/* If the state was just added, set its reducing actions */
						if ( stateAdded )
							for ( Item<?> item : addedState.items )
								if ( item.dotAtEnd () )
									for ( Terminal<?> followTerminal : item.lookaheads )
										/* If the follow terminal is the end terminal, and the head is the start symbol, then set this state to be accepting */
										if ( followTerminal == Terminal.end && item.core.head == grammar.start )
											addedState.addAcceptAction ( Terminal.end );
											/* Otherwise, add a reducing action */
										else
											addedState.addReduceAction ( followTerminal, item.core );

						/* We can add an action, if the symbol is a terminal.
						 * Otherwise, set the goto state.
						 */
						if ( symbol.isTerminal () )
							change |= state.addShiftAction ( ( Terminal<?> ) symbol, addedState );
						else
							change |= state.addGoto ( ( NonTerminal<?> ) symbol, addedState );
					}
				}

				/* Break if a state was added stateAdded */
				if ( stateAdded ) break;
			}
		} while ( change );

		/* Now we need to add error actions for remaining symbols */
		for ( State state : stateSet.values () )
		{
			for ( Terminal<?> terminal : grammar.terminals )
				if ( !state.actions.containsKey ( terminal ) )
					state.addErrorAction ( terminal );
			if ( !state.actions.containsKey ( Terminal.end ) )
				state.addErrorAction ( Terminal.end );
		}

		/* Return the start state */
		return startState;
	}


}
