import java.util.*;

/**
 * A class encapsulating a set of items.
 *
 * This is required, since items with the same core can be merged into one.
 *
 * The implementation is O(n), but I forgave this since the sets used will be fairly small, so the performance
 * gained by using a more efficient implementation will not be a huge benefit.
 */
public class ItemSet extends AbstractSet<Item<?>>
{

	/** The contents of the set */
	private final List<Item<?>> contents;



	/**
	 * Default constructor.
	 */
	public ItemSet ()
	{
		super ();
		contents = new LinkedList<> ();
	}

	/**
	 * @param c A collection to construct the set from.
	 */
	public ItemSet ( Collection<? extends Item<?>> c )
	{
		this ();
		addAll ( c );
	}


	/**
	 * @return An iterator for the set.
	 */
	@Override
	public Iterator<Item<?>> iterator ()
	{
		return contents.iterator ();
	}

	/**
	 * @return The size of the set.
	 */
	@Override
	public int size ()
	{
		return contents.size ();
	}



	/**
	 * @param item  An item whose core to look for in the set.
	 * @return  The item with a matching core, or null.
	 */
	private Item<?> matchCore ( Item<?> item )
	{
		/* Search for a matching core */
		for ( Item<?> elem : contents )
			if ( elem.core == item.core && elem.dot == item.dot )
				return elem;

		/* Not found, so return null */
		return null;
	}


	/**
	 * @param o The item to search for.
	 * @return  True if the item, along with all of its lookaheads, is contained in the set.
	 */
	@Override
	public boolean contains ( Object o )
	{
		Item<?> item = (Item<?>) o;
		Item<?> match = matchCore ( item );
		return match != null && match.lookaheads.containsAll ( item.lookaheads );
	}


	/**
	 * @param item  An item to add.
	 * @return  True if a change is made to the set.
	 */
	@Override
	public boolean add ( Item<?> item )
	{
		Item<?> match = matchCore ( item );
		if ( match == null )
			return contents.add ( new Item<> ( item ) );
		else
			return match.lookaheads.addAll ( item.lookaheads );
	}


	/**
	 * @param o An item to remove. Even if only some lookaheads are present, they are removed.
	 * @return  True if a change was made.
	 */
	@Override
	public boolean remove ( Object o )
	{
		/* Match the item's core */
		Item<?> item = (Item<?>) o;
		Item<?> match = matchCore ( item );

		/* Return if no match was made */
		if ( match == null ) return false;

		/* Remove lookaheads */
		boolean rt = match.lookaheads.removeAll ( item.lookaheads );

		/* If no lookaheads remain, remove the item */
		if ( match.lookaheads.isEmpty () )
			contents.remove ( match );

		/* Return whether changes occurred */
		return rt;
	}
}
