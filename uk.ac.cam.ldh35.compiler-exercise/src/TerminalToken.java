/**
 * A token whose symbol is known to be a terminal.
 *
 * @param <Res> The type of the token.
 */
public class TerminalToken<Res> extends Token<Res>
{
	/** The terminal for the token */
	public final Terminal<Res> symbol;



	/** The end token */
	public static final TerminalToken<Void> end = new TerminalToken<> ( Terminal.end, null );



	/**
	 * @param symbol The symbol the token is for.
	 * @param value  The value of the token.
	 */
	TerminalToken ( Terminal<Res> symbol, Res value )
	{
		super ( symbol, value );
		this.symbol = symbol;
	}

	/** A copy constructor */
	TerminalToken ( TerminalToken<Res> other )
	{
		super ( other );
		this.symbol = other.symbol;
	}
}
