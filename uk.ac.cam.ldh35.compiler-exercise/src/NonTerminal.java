import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * A class for a non-terminal grammar symbol.
 *
 * @param <Res> The resolution type of the GrammarSymbol.
 */
public class NonTerminal<Res> implements GrammarSymbol<Res>
{

	/** An interface for a reducer */
	public interface Reducer<Res>
	{
		Res reduce ( List<? extends Token<?>> body );
	}



	/**
	 * A production stores an array of grammar symbols, as well as a reducing function.
	 */
	public class Production
	{
		/** The head of the production */
		final NonTerminal<Res> head;

		/** The body of the production */
		public final List<GrammarSymbol<?>> body;

		/** The reducing function */
		public final Reducer<Res> reducer;



		/**
		 * @param body  The body of the production.
		 * @param reducer   A reducing function for the production.
		 */
		public Production ( List<? extends GrammarSymbol<?>> body, Reducer<Res> reducer )
		{
			/* Set the attributes */
			this.head = NonTerminal.this;
			this.body = new ArrayList<> ( body );
			this.reducer = reducer;
		}



		/**
		 * @param st    The input stack to match from.
		 * @return An object of type Token<Res> if matching was successful, otherwise null.
		 */
		public NonTerminalToken<Res> parseStack ( List<? extends Token<?>> st )
		{
			/* Compare the top of the stack with the symbols in the body */
			if ( body.size () > st.size () ) return null;
			for ( int i = 0; i < body.size (); ++i )
				if ( body.get ( body.size () - i - 1 ) != st.get ( st.size () - i - 1 ).symbol )
					return null;

			/* The stack matches, so extract those top tokens */
			List<? extends Token<?>> matched = st.subList ( st.size () - body.size (), st.size () );

			/* Reduce */
			NonTerminalToken<Res> result = new NonTerminalToken<> ( head, reducer == null ? null : reducer.reduce ( matched ) );

			/* Clear the matched tokens */
			matched.clear ();

			/* Return the token */
			return result;
		}


		/**
		 * @param dot   The dot position of the item.
		 * @param lookaheads    The lookaheads for the item.
		 * @return The new item.
		 */
		public Item<Res> createItem ( int dot, Set<Terminal<?>> lookaheads )
		{
			return new Item<> ( this, dot, lookaheads );
		}

		/**
		 * @param dot   The dot position of the item.
		 * @return The new item.
		 */
		public Item<Res> createItem ( int dot )
		{
			return new Item<> ( this, dot );
		}


		/**
		 * @return A string format of the production.
		 */
		@Override
		public String toString ()
		{
			if ( body.isEmpty () )
				return head.getName () + " -> " + Terminal.empty.getName ();
			else
				return head.getName () + " ->" + body.stream ().reduce ( "", ( str, elem ) -> str + " " + elem.getName (), ( lh, rh ) -> lh + rh );
		}
	}



	/** A name for the non-terminal */
	public final String name;

	/** The productions for the non-terminal */
	public final List<Production> productions = new ArrayList<> ();



	/** */
	public NonTerminal ( String name ) { this.name = name; }



	/**
	 * @return Whether the grammar symbol is a terminal.
	 */
	@Override
	public boolean isTerminal () { return false; }

	/**
	 * @return The name of the grammar symbol.
	 */
	@Override
	public String getName ()
	{
		return name;
	}

	/**
	 * @return The number of productions for the non-terminal.
	 */
	public int numProductions () { return productions.size (); }

	/**
	 * @param body  The body of the production.
	 * @param reducer   A reducing function for the production.
	 */
	public void addProduction ( List<? extends GrammarSymbol<?>> body, Reducer<Res> reducer )
	{
		productions.add ( new Production ( body, reducer ) );
	}

	/**
	 * @param body  The body of the production.
	 */
	public void addProduction ( List<? extends GrammarSymbol<?>> body )
	{
		productions.add ( new Production ( body, null ) );
	}


	/**
	 * @return The non-terminal formatted as a string.
	 */
	@Override
	public String toString ()
	{
		/* Switch depending on whether the non-terminal has any productions */
		if ( productions.isEmpty () )
		{
			/* Print just the name */
			return name + " (no productions)";
		} else
		{
			/* Print all productions with newlines between them */
			StringBuilder str = new StringBuilder ();
			for ( Production prod : productions )
				str.append ( prod.toString () ).append ( "\n" );
			return str.toString ();
		}
	}
}
